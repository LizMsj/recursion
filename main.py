def input_t():
    try:
        N = int(input("Введите натуральное число: "))
        check(N)
    except ValueError:
        print("Было получено некорректное значение.", end=' ')
        input_t()


def check(N):
    if N > 0:
        count(N)
    else:
        print("Было получено ненатуральное число.", end=' ')
        input_t()


def count(N, i=0):
    if N <= 0:
        print("Цифр в числе:", i)
        return i
    else:
        return count(N // 10, i + 1)


if __name__ == "__main__":
    input_t()
