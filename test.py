from main import count
import pytest


@pytest.mark.parametrize("N, expected", [(1, 1),
                                         (10, 2),
                                         (100, 3),
                                         (10000, 5),
                                         (1234567890, 10)])
def test_task_on_correct_result(N, expected):
    assert count(N) == expected


@pytest.mark.xfail(reason="негативный тест")
def test_task_on_failed_result():
    assert count(51, 0) == 51
